from django.apps import AppConfig


class MainbibConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "mainbib"
