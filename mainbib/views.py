from django.shortcuts import render
from django.template.response import TemplateResponse
from mainbib.models import Book, UserBook
from django.views.generic import ListView
from django.http import JsonResponse, HttpResponse

import requests
import urllib.parse

# Create your views here.

class BooksListView(ListView):
    model = Book

def my_books(request):
    user = request.user
    return TemplateResponse(request, "mybooks.html", {"books": UserBook.objects.filter(user=user)})

def my_profile(request):
    user = request.user
    return TemplateResponse(
        request,
        "myprofile.html", {
            "nb_books": len(UserBook.objects.filter(user=user))
        })

def my_validate(request, book_id):
    book = Book.objects.get(id = book_id)
    raw_query = f'intitle:{book.title}+inauthor:{book.author}'
    query = urllib.parse.quote(raw_query)
    url = f'https://www.googleapis.com/books/v1/volumes?q={query}&projection=lite&maxResults=1'

    payload = {}
    headers = {}

    response = requests.request("GET", url, headers=headers, data=payload)
    response_volume_info = response.json()['items'][0]['volumeInfo']
    response_title = response_volume_info['title']
    response_authors = response_volume_info['authors'][0]
    if response_title == book.title and response_authors == book.author:
        return HttpResponse(f'OK: Expected: {book}, API: {response_title}, {response_authors}')
    else:
        return HttpResponse(f'KO: Expected: {book}, API: {response_title}, {response_authors}')
    