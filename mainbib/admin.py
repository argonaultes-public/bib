from django.contrib import admin

from mainbib.models import Book, UserBook, Media

# Register your models here.

admin.site.register([Book, UserBook, Media])