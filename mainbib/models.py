from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
# Create your models here.

class Book(models.Model):
    title = models.CharField(max_length=200)
    content = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    available_in_store = models.BooleanField()
    store_price = models.FloatField()

    def __str__(self):
        return f'{self.title} ({self.author})'


class Media(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.name}'
    

class UserBook(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    media = models.ForeignKey(Media, on_delete=models.CASCADE)
    purchase_date = models.DateField()

    def __str__(self):
        return f'{self.book}: {self.user} ({self.media})'

