from django.test import TestCase

from mainbib.models import Book
# Create your tests here.

class BookTestCase(TestCase):

    databases = ['other']

    def setUp(self):
        Book.objects.create(title='test1', content='content', author='writer', available_in_store=False, store_price=12.2)
        Book.objects.create(title='test2', content='content', author='writer', available_in_store=True, store_price=11.2)

    def test_buy_book(self):
        pass

    def test_validate_book(self):

        pass