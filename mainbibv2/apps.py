from django.apps import AppConfig


class Mainbibv2Config(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "mainbibv2"
