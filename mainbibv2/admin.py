from django.contrib import admin

from mainbibv2.models import Book, Reader, Reading, Media
# Register your models here.

admin.site.register([Book, Media, Reading, Reader])
