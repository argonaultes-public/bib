# Generated by Django 5.0.1 on 2024-02-07 09:16

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("mainbibv2", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="reading",
            name="end_reading",
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name="reading",
            name="start_reading",
            field=models.DateField(null=True),
        ),
    ]
