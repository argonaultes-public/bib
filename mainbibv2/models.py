from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Media(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.name}'


class Reader(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    
    def __str__(self):
        return f'{self.user.username}'


class Book(models.Model):
    title = models.CharField(max_length=200)
    content = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    available_in_store = models.BooleanField()
    store_price = models.FloatField()
    readers = models.ManyToManyField(Reader, through="Reading")

    def __str__(self):
        return f'{self.title} ({self.author})'


class Reading(models.Model):
    media = models.ForeignKey(Media, on_delete=models.CASCADE)
    start_reading = models.DateField(null=True, blank=True)
    end_reading = models.DateField(null=True, blank=True)
    reader = models.ForeignKey(Reader, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)